> - Retail Price: 99 $ / 85 €
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) Price: 49,50 $ / 42,50 € - [Ask for the voucher](https://shop.ircam.fr/index.php?controller=contact)
> - Distributed by [Flux::](https://shop.flux.audio/en_US/products/ircam-verb-session)
> - [Technical Support](https://support.flux.audio/portal/sign_in)
> - Get you trial [Try](https://shop.flux.audio/en_US/page/trial-request-information)
> - Software available by downloading the [Flux:: Center ](https://www.flux.audio/download/)
> - iLok.com user account is required (USB Smart Key is not required)


![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/logos-flux%2Bircam-noir.png)

## Ircam Verb Session v3 – Algorithmic Reverb ##
**AU 32 & 64 bit | VST 32 & 64 bit | AAX Native 32 & 64 bit* **

Tailored for simplicity, with a fast paced workflow well-suited for situations where the perfect result has to be achieved within seconds, VERB Session v3 presents the ultimate solution whether you are a seasoned session engineer or a demanding broadcast and post-production mixer.

## Features ##

- Up to 2 channels Input/Output.
- Input/Output gain controls for adjusting the levels before and after processing.
- Dry/Wet control for blending the original signal with the processed signal.
- Decay Time adjusting the duration of the reverberation tail in seconds.

### Decay Time ###

The Decay Time, is the duration of the reverberation tail in seconds, the time it takes for the reverberated sound to die out. In technical terms this is often referred to as the RT60 factor, the time at which the response of the reverberation to an input signal goes below -60dB of attenuation.

### Pre Delay ###

The Pre Delay controls the time at which the reverberation portion of the effect starts to be noticed, with respect to the DIRECT signal. Increasing this helps to distinguish between direct and effected sound and preserve intelligibility, especially with large decay times and room sizes which would otherwise drench the audio material in reverberation.

### Room Size ###

The Room Size is a meta parameter used to quickly perform a homogeneous set of parameters for the early reflections part (early + cluster). These parts are particularly important to achieve the “room” feeling of the desired space. It adjust the time structure of the whole reverberation (early-min, early-max, cluster-min, cluster-max, reverb-start).

### Filter Section ###

The Filter Section controls the characteristics of the filter applied to the signal fed to the reverberation engine, affecting the overall frequency response, if required.
- Low Freq - Low pass filter frequency cutoff of the corresponding filter section.
- High Freq - High pass filter frequency cutoff of the corresponding filter section

### Dual Preset Slots and Parameter Morphing ###

The built in preset manager and the preset morphing slider, provides instant and intuitive control of all parameters and controls. In a second, with a simple one-click operation, everything is copied from one of the two preset slots to the other, even during playback.

### Time Structure Gain and Damping ###

- Early Gain - Controls the level of EARLY and CLUSTER contributed to the overall effect.
- Tail Gain - Controls the level of LATE/TAIL contributed to the overall effect.
- Low Damping - Adjusts low-frequency damping, for increasing or decreasing the decay of bass content with respect to the rest of the spectrum
- High Damping - Adjusts high frequency content damping.

![Iracam Verb Session](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/ircam_verb_session.png)

## Compatibility & plugin formats ##

- **Windows** - 7 SP1, 8.1 and 10, all in 64 bits only
- **Mac** OS X (Intel) - All versions from 10.7.5 (64bit only)
- **Ircam Verb Session v3** is available in the following configurations: AudioUnit (64bit) / VST 2 (64 bit) / VS3 Native+Masscore (Pyramix 10-12) / AAX Native (64bit)
- **Max sample rate (depending on HOST)**: 384 KHz
- **Maximum number of channels**: 2

## Hardware Specification ##

A graphic card fully supporting OpenGL 2.0 is required.

- **Windows:** If your computer has an ATi or NVidia graphics card, please assure the latest graphic drivers from the ATi or NVidia website are installed.
- **Mac OS X:** OpenGL 2.0 required - Mac Pro 1.1 & Mac Pro 2.1 are not supported.

## Software license requirements ##

In order to use the software, an iLok.com user account is required (the iLok USB Smart Key is not required).

VS3 license not included in Flux:: standard license. Additional VS3 license for Pyramix & Ovation Native/MassCore 32/64 bit versions available from [Merging Technologies](https://www.merging.com/sales).
